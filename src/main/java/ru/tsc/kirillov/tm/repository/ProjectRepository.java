package ru.tsc.kirillov.tm.repository;

import ru.tsc.kirillov.tm.api.repository.IProjectRepository;
import ru.tsc.kirillov.tm.model.AbstractModel;
import ru.tsc.kirillov.tm.model.Project;

public class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @Override
    public Project create(final String userId, final String name) {
        final Project project = new Project();
        project.setName(name);
        project.setUserId(userId);
        return add(project);
    }

    @Override
    public Project create(final String userId, final String name, final String description) {
        final Project project = new Project();
        project.setName(name);
        project.setUserId(userId);
        project.setDescription(description);
        return add(project);
    }

    @Override
    public String[] findAllId(final String userId) {
        return findAll(userId)
                .stream()
                .map(AbstractModel::getId)
                .toArray(String[]::new);
    }

}
