package ru.tsc.kirillov.tm.repository;

import ru.tsc.kirillov.tm.api.repository.ITaskRepository;
import ru.tsc.kirillov.tm.model.Task;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    protected Predicate<Task> predicateByUserIdAndProjectId(final String userId, final String projectId) {
        return task -> task.getProjectId() != null
                && userId.equals(task.getUserId())
                && projectId.equals(task.getProjectId());
    }

    @Override
    public Task create(final String userId, final String name) {
        final Task task = new Task();
        task.setName(name);
        task.setUserId(userId);
        return add(task);
    }

    @Override
    public Task create(final String userId, final String name, final String description) {
        final Task task = new Task();
        task.setName(name);
        task.setUserId(userId);
        task.setDescription(description);
        return add(task);
    }

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        return models
                .stream()
                .filter(predicateByUserIdAndProjectId(userId, projectId))
                .collect(Collectors.toList());
    }

    @Override
    public void removeAllByProjectId(final String userId, final String projectId) {
        findAllByProjectId(userId, projectId)
            .stream()
            .forEach(t -> removeById(userId, t.getId()));
    }

    @Override
    public void removeAllByProjectList(final String userId, final String[] projects) {
        Stream.of(projects)
                .filter(p -> p != null && !p.isEmpty())
                .forEach(p -> removeAllByProjectId(userId, p));
    }

}
