package ru.tsc.kirillov.tm.exception.field;

public final class EmailEmptyException extends AbstractFieldException {

    public EmailEmptyException() {
        super("Ошибка! E-mail не задан.");
    }

}
