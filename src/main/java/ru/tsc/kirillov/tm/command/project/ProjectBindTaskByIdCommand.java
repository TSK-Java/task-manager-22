package ru.tsc.kirillov.tm.command.project;

import ru.tsc.kirillov.tm.util.TerminalUtil;

public final class ProjectBindTaskByIdCommand extends AbstractProjectCommand {

    @Override
    public String getName() {
        return "project-bind-task-by-id";
    }

    @Override
    public String getDescription() {
        return "Привязать задачу к проекту.";
    }

    @Override
    public void execute() {
        System.out.println("[Привязка задачи к проекту]");
        System.out.println("Введите ID проекта:");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("Введите ID задачи:");
        final String taskId = TerminalUtil.nextLine();
        getProjectTaskService().bindTaskToProject(getUserId(), projectId, taskId);
    }

}
