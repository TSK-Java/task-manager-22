package ru.tsc.kirillov.tm.command;

import ru.tsc.kirillov.tm.api.model.ICommand;
import ru.tsc.kirillov.tm.api.service.IAuthService;
import ru.tsc.kirillov.tm.api.service.IServiceLocator;
import ru.tsc.kirillov.tm.enumerated.Role;

public abstract class AbstractCommand implements ICommand {

    protected IServiceLocator serviceLocator;

    protected IAuthService getAuthService() {
        return getServiceLocator().getAuthService();
    }

    protected String getUserId() {
        return getAuthService().getUserId();
    }

    public abstract String getName();

    public String getArgument() {
        return null;
    }

    public abstract String getDescription();

    public abstract void execute();

    public abstract Role[] getRoles();

    public IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    public void setServiceLocator(final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public String toString() {
        String result = "";
        final String name = getName();
        final String argument = getArgument();
        final String description = getDescription();

        if (name != null && !name.isEmpty())
            result += name;
        if (argument != null && !argument.isEmpty())
            result += ", " + argument;
        if (description != null && !description.isEmpty())
            result += " - " + description;

        if (!result.isEmpty())
            return result;
        else
            return super.toString();
    }

}
