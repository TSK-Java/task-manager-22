package ru.tsc.kirillov.tm.command.task;

import ru.tsc.kirillov.tm.enumerated.Sort;
import ru.tsc.kirillov.tm.util.TerminalUtil;

import java.util.Arrays;

public final class TaskListCommand extends AbstractTaskListCommand {

    @Override
    public String getName() {
        return "task-list";
    }

    @Override
    public String getDescription() {
        return "Отображение списка задач.";
    }

    @Override
    public void execute() {
        System.out.println("[Список задач]");
        System.out.println("Введите способ сортировки");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        printTask(getTaskService().findAll(getUserId(), sort));
    }

}
