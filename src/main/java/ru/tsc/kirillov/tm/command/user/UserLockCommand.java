package ru.tsc.kirillov.tm.command.user;

import ru.tsc.kirillov.tm.enumerated.Role;
import ru.tsc.kirillov.tm.util.TerminalUtil;

public final class UserLockCommand extends AbstractUserCommand {

    @Override
    public String getName() {
        return "user-lock";
    }

    @Override
    public String getDescription() {
        return "Блокировка пользователя.";
    }

    @Override
    public void execute() {
        System.out.println("[Блокировка пользователя]");
        System.out.println("Введите логин пользователя:");
        final String login = TerminalUtil.nextLine();
        getUserService().lockUserByLogin(login);
    }

    @Override
    public Role[] getRoles() {
        return new Role[] { Role.ADMIN };
    }

}
