package ru.tsc.kirillov.tm.command.user;

import ru.tsc.kirillov.tm.enumerated.Role;
import ru.tsc.kirillov.tm.util.TerminalUtil;

public final class UserUnlockCommand extends AbstractUserCommand {

    @Override
    public String getName() {
        return "user-unlock";
    }

    @Override
    public String getDescription() {
        return "Разблокировка пользователя.";
    }

    @Override
    public void execute() {
        System.out.println("[Разблокировка пользователя]");
        System.out.println("Введите логин пользователя:");
        final String login = TerminalUtil.nextLine();
        getUserService().unlockUserByLogin(login);
    }

    @Override
    public Role[] getRoles() {
        return new Role[] { Role.ADMIN };
    }

}
