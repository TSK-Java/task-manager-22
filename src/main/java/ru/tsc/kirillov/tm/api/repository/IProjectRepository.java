package ru.tsc.kirillov.tm.api.repository;

import ru.tsc.kirillov.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    Project create(String userId, String name);

    Project create(String userId, String name, String description);

    String[] findAllId(String userId);

}
